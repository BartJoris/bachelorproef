  /usr/share/nginx/www:
   file.directory:
     - user: vagrant
     - group: vagrant
     - mode: 664
     - makedirs: True

  /usr/share/nginx/www/index.html:
   file.managed:
     - source: salt://index.html
     - user: vagrant
     - group: vagrant
     - mode: 644