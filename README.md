#Bachelorproef Bart Joris
###Een vergelijkende studie tussen de configuration management en orchestration tools: Ansible en SaltStack


Hier vind je al de gebruikte bestanden van mijn Scriptie. Deze bestanden kunnen getest worden in een virtuele omgeving dat is opgezet via Vagrant. Enkel om SaltStack als provisioner te gebruiken dien je een extra plugin te installeren.

####De SaltStack plugin kan op volgende manier worden geïnstalleerd: 
```sh
vagrant plugin install vagrant-salt
```
Bron: [Salty Vagrant](https://github.com/saltstack/salty-vagrant)

####Aanmaken virtuele machines: 
#####Ansible
```sh
cd Ansible
vagrant up
```
#####SaltStack
```sh
cd SaltStack
vagrant up
```
Extra informatie over Vagrant: [Vagrant](https://www.vagrantup.com)

